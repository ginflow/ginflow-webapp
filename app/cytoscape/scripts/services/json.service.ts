import { Injectable, EventEmitter } from '@angular/core';

import { GraphService } 			from './graph.service';

import { Service } 					from '../service';
import { TypeControl } 				from '../type-control';

@Injectable()
export class JsonService {
	/**
	 * Constructor.
	 * Injects an instance of {@link GraphService}.
	 * @class JsonService
	 * @classdesc Service which will take care of import/export/format JSON data.
	 * {@link GraphService} is used to get nodes and edges of the graph because this class owns the core instance of Cytoscape ({@link GraphService#cy}).
	 */
	constructor(private graph : GraphService) {}

	/**
	 * Extract JSON from nodes and format into a good representation for Ginflow.
	 * @method JsonService#getJsonFromNodes
	 * @private
	 * @returns {any} The JSON representation of nodes in the good format
	 */
	private getJsonFromNodes() : any {
		// Get the JSON object from the nodes of the graph
		var services_json: any = this.graph.getNodes().jsons();
		
		var services_array = [];

		// Loop over the nodes of the graph
		for(var i = 0; i < services_json.length; i++) {
			// Put service of each node in an array
			services_array.push(services_json[i].data.service);
		}

		// Loop over services
		for(var i = 0; i < services_array.length; i++) {
			// The in attribute in the current service is an array
			if(Array.isArray(services_array[i].in)) {
				// Transform this array by a string with commas
				services_array[i].in = services_array[i].in.join();
			}
		}

		return services_array;
	}

	/**
	 * Extract JSON from nodes and format into a good representation for Ginflow.
	 * @method JsonService#getJsonFromNodesSelected
	 * @private
	 * @param {any} nodes The nodes
	 * @returns {any} The JSON representation of nodes in the good format
	 */
	private getJsonFromNodesSelected(nodes : any) {
		// Get the JSON object from the nodes of the graph
		var services_json: any = nodes.jsons();
		
		var services_array = [];

		// Loop over the nodes of the graph
		for(var i = 0; i < services_json.length; i++) {
			// Put service of each node in an array
			services_array.push(services_json[i].data.service);
		}

		// Loop over services
		for(var i = 0; i < services_array.length; i++) {
			// The in attribute in the current service is an array
			if(Array.isArray(services_array[i].in)) {
				// Transform this array by a string with commas
				services_array[i].in = services_array[i].in.join();
			}
		}

		return services_array;
	}

	/**
	 * Convert the entire graph data cytoscape into a ginflow JSON representation.
	 * @method JsonService#cytoscapeGraphToGinflowWorkspaceJson
	 * @param {string} workflowName The name of the workflow
	 * @returns {any} The Ginflow JSON representation
	 */
	public cytoscapeGraphToGinflowWorkspaceJson(workflowName : string) : any {
		// Get JSON object from nodes of the graph
		var services_json = this.getJsonFromNodes();

		// Loop over services
		for(var i = 0; i < services_json.length; i++) {
			var service = services_json[i];
			// in attribute of the current service is empty
			if(service.in.length == 0) {
				// Force in to be ""
				service.in = "";
			}

			// Current service has a well-defined alternative group
			if(!isNaN(parseInt(service.alt))) {
				// Convert string into number
				service.alt = parseInt(service.alt);
			}
			// Otherwise, set alt undefined
			else {
				service.alt = undefined;
			}

			// Current service has a well-defined supervised group
			if(!isNaN(parseInt(service.sup))) {
				// Convert string into number
				service.sup = parseInt(service.sup);
			}
			// Otherwise, set sup undefined
			else {
				service.sup = undefined;
			}
		}

		/*
		 * Map to :
		 * 		- Set name as an array
		 * 		- Set srv as an array
		 * 		- Set in as an array
		 * 		- Extract id from src, dst, src_control and dst_control
		 */
		var services = services_json.map(function(service) {
			return {
				name: [service.name],
				srv: [service.srv],
				in: service.in.split(" "),
				alt: service.alt,
				sup: service.sup,
				src: service.src.map(source => {
					return source.id.toString()
				}),
				dst: service.dst.map(destination => {
					return destination.id.toString()
				}),
				src_control: service.srcControl.map(sourceControl => {
					return sourceControl.id.toString()
				}),
				dst_control: service.dstControl.map(destinationControl => {
					return destinationControl.id.toString()
				})
			}
		});

		/* Build the JSON object for ginflow.
		 * Remap the in attribute if:
		 * 		-> in is an array which contains just [""] and set it to [] in this case
		 */
		var workflow_json = {
			"name" : workflowName,
			"services" : services.map(service => {
				if(service.in.length == 1 && service.in[0] == "") {
					service.in = [];
				}
				return service;
			})
		};
		return workflow_json;
	}

	/**
	 * Convert the entire graph data cytoscape into a ginflow JSON representation.
	 * @method JsonService#cytoscapeGraphUpdatedToGinflowWorkspaceJson
	 * @param {string} workflowName The name of the workflow
	 * @param {any} nodes The nodes
	 * @returns {any} The Ginflow JSON representation
	 */
	public cytoscapeGraphUpdatedToGinflowWorkspaceJson(workflowName : string, nodes : any) : any {
		// Get JSON object from nodes of the graph
		var services_json = this.getJsonFromNodesSelected(nodes);

		// Loop over services
		for(var i = 0; i < services_json.length; i++) {
			var service = services_json[i];
			// in attribute of the current service is empty
			if(service.in.length == 0) {
				// Force in to be ""
				service.in = "";
			}

			// Current service has a well-defined alternative group
			if(!isNaN(parseInt(service.alt))) {
				// Convert string into number
				service.alt = parseInt(service.alt);
			}
			// Otherwise, set alt undefined
			else {
				service.alt = undefined;
			}

			// Current service has a well-defined supervised group
			if(!isNaN(parseInt(service.sup))) {
				// Convert string into number
				service.sup = parseInt(service.sup);
			}
			// Otherwise, set sup undefined
			else {
				service.sup = undefined;
			}
		}

		/*
		 * Map to :
		 * 		- Set name as an array
		 * 		- Set srv as an array
		 * 		- Set in as an array
		 * 		- Extract id from src, dst, src_control and dst_control
		 */
		var services = services_json.map(function(service) {
			return {
				name: [service.name],
				srv: [service.srv],
				in: service.in.split(" "),
				alt: service.alt,
				sup: service.sup,
				src: service.src.map(source => {
					return source.id.toString()
				}),
				dst: service.dst.map(destination => {
					return destination.id.toString()
				}),
				src_control: service.srcControl.map(sourceControl => {
					return sourceControl.id.toString()
				}),
				dst_control: service.dstControl.map(destinationControl => {
					return destinationControl.id.toString()
				})
			}
		});

		/* Build the JSON object for ginflow.
		 * Remap the in attribute if:
		 * 		-> in is an array which contains just [""] and set it to [] in this case
		 */
		var workflow_json = {
			"name" : workflowName,
			"services" : services.map(service => {
				if(service.in.length == 1 && service.in[0] == "") {
					service.in = [];
				}
				return service;
			})
		};
		return workflow_json;
	}

	/**
	 * Utility method which format cytoscape graph to JSON object.
	 * @method JsonService#graphToJson
	 * @param {string} workflow The name of the workflow 
	 * @param {any} services The services
	 * @returns {any} the JSON data
	 */
	public graphToJson(workflow : string, services : any) : any {
		// Object JSON
		var json = {
			"name" : workflow,
			"services" : services.map(service => { // Map to format some data in the services
				return {
					service: {
						id: service.data('service').id.toString(),
						name: service.data('service').name,
						srv: service.data('service').srv,
						in: service.data('service').in,
						alt: service.data('service').alt,
						sup: service.data('service').sup,

						// We only want id's for dependencies but not the entire Service
						src: service.data('service').src.map(source => {
							return source.id.toString()
						}),
						dst: service.data('service').dst.map(destination => {
							return destination.id.toString()
						}),
						src_control: service.data('service').srcControl.map(sourceControl => {
							return sourceControl.id.toString()
						}),
						dst_control: service.data('service').dstControl.map(destinationControl => {
							return destinationControl.id.toString()
						}),
						description: service.data('service').description
					},
					position: service.position()
				}

			}) 
		};
		return json;
	}

	/**
	 * Utility method used to create graph by importing JSON text.
	 * @method JsonService#createGraphOnImport
	 * @param {any} data The data of the JSON text
	 */
	public createGraphOnImport(data : any) {
		var workspace_name = data.name;
		var services = data.services;

		// Iterate over services
		for(var i = 0; i < services.length; i++) {
			var id = services[i].service.id;
			var name = services[i].service.name;
			var srv = services[i].service.srv;
			var in_param = services[i].service.in;
			var alt = services[i].service.alt;
			var sup = services[i].service.sup;

			// First, create a new instance of the Service without dependencies
			var service = new Service(id)	.setName(name)
											.setSrv(srv)
											.setIn(in_param)
											.setAlt(alt)
											.setSup(sup)
											.setDescription("service with name : " + name);

			var position = services[i].position;
			
			// Add the node to the graph by specifying its informations and its position
			this.graph.addNode(id, service, position);
		}

		// Iterate again over services, this time to update their dependencies
		for(var i = 0; i < services.length; i++) {
			var node = this.graph.getNodeById(services[i].service.id);
			var srcs = services[i].service.src;
			var dsts = services[i].service.dst;
			var src_controls = services[i].service.srcControl;
			var dst_controls = services[i].service.dstControl;

			// Set dependencies of each service
			node.data('service').setSrcs(srcs)
								.setDsts(dsts)
								.setSrcControls(src_controls)
								.setDstControls(dst_controls);

			// If the set of sources of the current service is not empty
			if(node.data('service').getSrcs() != undefined) {
				// Iterate over the set of sources
				for(var j = 0; j < node.data('service').getSrcs().length; j++) {
					var src = node.data('service').getSrcs()[j];
					// If the edge has not been created yet, we create it
					if(this.graph.cy.getElementById("e" + src + "-" + node.id()).length == 0) {
						/*
						 * Add the edge to the graph
						 * Specify its id, source, target and its type of dependency (here data dependency)
						 */
						this.graph.cy.add({
							group: "edges",
							data: {
								id: "e" + src + "-" + node.id(),
								source: src,
								target: node.id(),
								type_control: TypeControl.Data
							}
						});
					}
				}
			}

			// If the set of destinations of the current service is not empty
			if(node.data('service').getDsts() != undefined) {
				// Iterate over the set of destinations
				for(var j = 0; j < node.data('service').getDsts().length; j++) {
					var dst = node.data('service').getDsts()[j];
					// If the edge has not been created yet, we create it
					if(this.graph.cy.getElementById("e" + node.id() + "-" + dst).length == 0) {
						/*
						 * Add the edge to the graph
						 * Specify its id, source, target and its type of dependency (here data dependency)
						 */
						this.graph.cy.add({
							group: "edges",
							data: {
								id: "e" + node.id() + "-" + dst,
								source: node.id(),
								target: dst,
								type_control: TypeControl.Data
							}
						});
					}
				}
			}

			// If the set of sources (control) of the current service is not empty
			if(node.data('service').getSrcControls() != undefined) {
				// Iterate over the set of sources (control)
				for(var j = 0; j < node.data('service').getSrcControls().length; j++) {
					var src_control = node.data('service').getSrcControls()[j];
					// If the edge has not been created yet, we create it
					if(this.graph.cy.getElementById("e" + src_control + "-" + node.id()).length == 0) {
						/*
						 * Add the edge to the graph
						 * Specify its id, source, target and its type of dependency (here control dependency)
						 */
						this.graph.cy.add({
							group: "edges",
							data: {
								id: "e" + src_control + "-" + node.id(),
								source: src_control,
								target: node.id(),
								type_control: TypeControl.Control
							}
						});
					}
				}
			}

			// If the set of destinations (control) of the current service is not empty
			if(node.data('service').getDstControls() != undefined) {
				// Iterate over the set of destinations (control)
				for(var j = 0; j < node.data('service').getDstControls().length; j++) {
					var dst_control = node.data('service').getDstControls()[j];
					// If the edge has not been created yet, we create it
					if(this.graph.cy.getElementById("e" + node.id() + "-" + dst_control).length == 0) {
						/*
						 * Add the edge to the graph
						 * Specify its id, source, target and its type of dependency (here control dependency)
						 */
						this.graph.cy.add({
							group: "edges",
							data: {
								id: "e" + node.id() + "-" + dst_control,
								source: node.id(),
								target: dst_control,
								type_control: TypeControl.Control
							}
						});
					}
				}
			}
		}
	}
}