import { Injectable } 			from '@angular/core';

import { GraphService }			from './graph.service';
import { TypeControl }			from '../type-control';

@Injectable()
export class StyleUpdaterService {
	/**
	 * Constructor.
	 * Injects an instance of {@link GraphService}.
	 * @class StyleUpdaterService
	 * @classdesc Service which will take care of styling elements when the state of the graph change.
	 * It will update css style on elements in the graph.
	 * It uses an instance of {@link GraphService} to be able to get elements in this graph.
	 * Because {@link GraphService} owns the core instance of Cytoscape ({@link GraphService#cy}).
	 */
	constructor(private graph : GraphService) {} 

	/**
	 * Update dynamically the font size of the node label.
	 * The node label content changes when we update the srv field in the form in AppComponent.
	 * Updates the font size to prevent the text from overflowing on the node.
	 * @method StyleUpdaterService#updateFontSizeNodeLabel
	 * @param {any} changes The array containing all the inputs in GraphComponent
	 * @param {any} change The input which concern the node label (a.k.a. currentSrv in GraphComponent)
	 */
	public updateFontSizeNodeLabel(changes: any, change: any) {
		// Get the id of the selected node
		var id = this.graph.getNodesBySelector('.selected-node').id();

		// Get the node which has the id found before
		var element = this.graph.getNodeById(id);
		
		// Initialize the font size
		element.css("font-size", 30);

		// Get the current value of the node label
		var length = changes[change].currentValue.length;

		// Convert string (for example '10px') into a number (10)
		var font_size_style = element.style("font-size");
		var font_size_string = font_size_style.substring(0, font_size_style.length - 2);
		var font_size = parseInt(font_size_string);

		// Adapt the size accordingly to the length of the label
		font_size = 30 - Math.floor( length / 5) * 3;

		// Apply the new font size in the label
		element.css("font-size", font_size);
	}

	/**
	 * Remove all css classes which concern the state selected of elements (i.e. nodes/edges).
	 * This will deselect visually the elements in the graph.
	 * @method StyleUpdaterService#clearStyleSelectedItems
	 */
	public clearStyleSelectedItems() {
		this.clearStyleSelectedNodes();
		this.clearStyleSelectedEdges();
	}

	/**
	 * Remove all css classes which concern the state selected of nodes.
	 * This will deselect visually the nodes in the graph.
	 * @method StyleUpdaterService#clearStyleAllSelectedNodes
	 */
	public clearStyleAllSelectedNodes() {
		this.clearStyleSelectedNodes();
		this.clearStyleFailedSelectedNodes();
		this.clearStyleReadySelectedNodes();
		this.clearStyleDeployedSelectedNodes();
		this.clearStyleValidatedSelectedNodes();
	}

	/**
	 * Remove css class which concern selected nodes before the execution of the nodes.
	 * This will deselect visually the nodes, which are not yet executed, in the graph.
	 * @method StyleUpdaterService#clearStyleSelectedNodes
	 */
	public clearStyleSelectedNodes() {
		this.graph.getNodes().removeClass('selected-node');
	}

	/**
	 * Remove css class which concern selected nodes which have a failure during the execution (i.e. exit code > 0).
	 * This will deselect visually the nodes, which have a failure, in the graph.
	 * @method StyleUpdaterService#clearStyleFailedSelectedNodes
	 */
	public clearStyleFailedSelectedNodes() {
		this.graph.getNodes().removeClass('selected-failed-node');
	}

	/**
	 * Remove css class which concern selected nodes which have just sent their description during the execution.
	 * This will deselect visually the nodes, which have a just sent their description, in the graph.
	 * @method StyleUpdaterService#clearStyleReadySelectedNodes
	 */
	public clearStyleReadySelectedNodes() {
		this.graph.getNodes().removeClass('selected-ready-node');
	}

	/**
	 * Remove css class which concern selected nodes which have been deployed during the execution.
	 * This will deselect visually the nodes, which have been deployed, in the graph.
	 * @method StyleUpdaterService#clearStyleDeployedSelectedNodes
	 */
	public clearStyleDeployedSelectedNodes() {
		this.graph.getNodes().removeClass('selected-deployed-node');
	}

	/**
	 * Remove css class which concern selected nodes which have been executed with no error during the execution (i.e. exit code = 0).
	 * This will deselect visually the nodes, which have a been executed with no error, in the graph.
	 * @method StyleUpdaterService#clearStyleValidatedSelectedNodes
	 */
	public clearStyleValidatedSelectedNodes() {
		this.graph.getNodes().removeClass('selected-validated-node');
	}

	/**
	 * Remove css class which concern selected edges.
	 * This will deselect visually the nodes in the graph.
	 * @method StyleUpdaterService#clearStyleSelectedEdges
	 */
	public clearStyleSelectedEdges() {
		this.graph.getEdges().removeClass('selected-edge');
	}

	/**
	 * Update the style of edges when the type of these edges is 'Control'.
	 * Add css class 'edge-control' to these edges.
	 * @method StyleUpdaterService#updateStyleEdgesControl
	 */
	public updateStyleEdgesControl() {
		// Loop over the edges of the graph
		for(var i = 0; i < this.graph.getEdges().length; i++) {
			// The edge has the type 'Control'
			if(this.graph.getEdges()[i].data('type_control') == TypeControl.Control) {
				this.graph.getEdges()[i].addClass('edge-control');
			}
		}
	}

	/**
	 * Update edges which are connected to at least one node which is an alternative.
	 * This will transform solid lines in dashed lines.
	 * @method StyleUpdaterService#updateStyleEdgesAlternative
	 */
	public updateStyleEdgesAlternative() {
		// Loop over nodes of the graph
		for(var i = 0; i < this.graph.getNodes().length; i++) {
			// Get the current node
			var currentService = this.graph.getNodes()[i];

			// Get all its connected edges
			var connectedEdges = currentService.connectedEdges();

			// Loop over its connected edges
			for(var j = 0; j < connectedEdges.length; j++) {
				// Get the source node of the current edge
				var source = connectedEdges[j].source();

				// Get the target node of the current edge				
				var target = connectedEdges[j].target();

				// If the target node does not exist
				if(target == null) {
					// If the source node of this edge belongs to a well-defined alternative group
					if(source.data('service').alt != undefined && source.data('service').alt != "") {
						// We set a flag 'alternative' to this edge, to mark this edge as an alternative for later
						connectedEdges[j].data('alternative', true);
					}
					// The source node of this edge has no alternative group
					else {
						// The flag 'alternative' is false
						connectedEdges[j].data('alternative', false);
					}
				}
				// If the source node does not exist
				else if(source == null) {
					// If the target node of this edge belongs to a well-defined alternative group
					if(target.data('service').alt != undefined && target.data('service').alt != "") {
						// We set a flag 'alternative' to this edge, to mark this edge as an alternative for later
						connectedEdges[j].data('alternative', true);
					}
					// The target node of this edge has no alternative group
					else {
						// The flag 'alternative' is false
						connectedEdges[j].data('alternative', false);
					}
				}
				// If the source node and the target node is set
				else if(source != null && target != null) {
					// One of them (source or target or both) have a well-defined alternative group
					if(	(target.data('service').alt != undefined && target.data('service').alt != "") || 
						(source.data('service').alt != undefined && source.data('service').alt != "")) {
						// We set a flag 'alternative' to this edge, to mark this edge as an alternative for later
						connectedEdges[j].data('alternative', true);
					}
					// None of the target node and the source node of this edge have a well-defined alternative group
					else {
						// The flag 'alternative' is false
						connectedEdges[j].data('alternative', false);
					}
				}
			}
			// We add css class 'alternative-edge' to edges which have the flag 'alternative' to true
			this.graph.cy.edges('[?alternative]').addClass('alternative-edge');

			// We remove css class 'alternative-edge' to edges which have the flag 'alternative' to false
			this.graph.cy.edges('[!alternative]').removeClass('alternative-edge');
		}
	}

	/**
	 * Hide all alternatives (nodes and edges) of the graph.
	 * @method StyleUpdaterService#hideAlternatives
	 */
	public hideAlternatives() {
		// Loop over nodes of the graph
		for(var i = 0; i < this.graph.getNodes().length; i++) {
			// The current node has a well-defined alternative group
			if(!isNaN(this.graph.getNodes()[i].data('service').alt)) {
				// Get the connected edges of this node
				var connectedEdges = this.graph.getNodes()[i].connectedEdges();

				/*
				 * Perform animation on this node
				 * Opacity will decrease progressively (1 to 0)
				 * Changing opacity is the trick instead of visibility: hidden for the animation
				 */
				this.graph.getNodes()[i].animate({
					css: {
						'opacity': 0
					}
				},
				{
					duration: 1000
				});

				/*
				 * Perform animation on connected edges
				 * Opacity will decrease progressively (1 to 0)
				 */
				connectedEdges.animate({
					css: {
						'opacity': 0
					}
				},
				{
					duration: 1000
				});
			}
		}
	}

	/**
	 * Hide all alternatives (nodes and edges) of the graph.
	 * @method StyleUpdaterService#hideAlternative
	 * @param {number} sup The supervised group
	 */
	public hideAlternative(sup : number) {
		// Loop over nodes of the graph
		for(var i = 0; i < this.graph.getNodes().length; i++) {
			// The current node has a well-defined alternative group
			if(!isNaN(this.graph.getNodes()[i].data('service').alt) && this.graph.getNodes()[i].data('service').alt == sup) {
				// Get the connected edges of this node
				var connectedEdges = this.graph.getNodes()[i].connectedEdges();

				/*
				 * Perform animation on this node
				 * Opacity will decrease progressively (1 to 0)
				 * Changing opacity is the trick instead of visibility: hidden for the animation
				 */
				this.graph.getNodes()[i].animate({
					css: {
						'opacity': 0
					}
				},
				{
					duration: 1000
				});

				/*
				 * Perform animation on connected edges
				 * Opacity will decrease progressively (1 to 0)
				 */
				connectedEdges.animate({
					css: {
						'opacity': 0
					}
				},
				{
					duration: 1000
				});
			}
		}
	}

	/**
	 * Show alternatives which belong to the supervised group sup.
	 * @method StyleUpdaterService#showAlternatives
	 * @param {number} sup The supervised group
	 */
	public showAlternatives(sup : number) {
		// Loop over nodes of the graph
		for(var i = 0; i < this.graph.getNodes().length; i++) {
			// Get the alternative group of the current node
			var alternative = this.graph.getNodes()[i].data('service').alt;
			
			// The alternative group of this node is well-defined and has the same number as the supervised group
			if(!isNaN(alternative) && alternative == sup) {

				// Get connected edges of this node
				var connectedEdges = this.graph.getNodes()[i].connectedEdges();

				/*
				 * Perform animation on this node
				 * Opacity will increase progressively (0 to 1)
				 * Changing opacity is the trick instead of visibility: element for the animation
				 */
				this.graph.getNodes()[i].animate({
					css: {
						'opacity': 1
					}
				},
				{
					duration: 1000
				});

				/*
				 * Perform animation on connected edges
				 * Opacity will increase progressively (0 to 1)
				 */
				connectedEdges.animate({
					css: {
						'opacity': 1
					}
				},
				{
					duration: 1000
				});
			}
		}
	}

	/**
	 * Update alternatives of the current service, by changing css.
	 * @method StyleUpdaterService#updateAlternativesCurrentService
	 * @param {any} currentService The current service
	 */
	public updateAlternativesCurrentService(currentService: any) {
		var connectedEdges = currentService.connectedEdges();
		for(var i = 0; i < connectedEdges.length; i++) {
			var source = connectedEdges[i].source();
			var target = connectedEdges[i].target();

			if(target == null) {
				if(source.data('service').alt != undefined && source.data('service').alt != "") {
					connectedEdges[i].data('alternative', true);
				}
				else {
					connectedEdges[i].data('alternative', false);
				}
			}
			else if(source == null) {
				if(target.data('service').alt != undefined && target.data('service').alt != "") {
					connectedEdges[i].data('alternative', true);
				}
				else {
					connectedEdges[i].data('alternative', false);
				}
			}
			else if(source != null && target != null) {
				if(
					(target.data('service').alt != undefined && target.data('service').alt != "")
					|| 
					(source.data('service').alt != undefined && source.data('service').alt != "")
				) {
					connectedEdges[i].data('alternative', true);
				}
				else {
					connectedEdges[i].data('alternative', false);
				}
			}
		}
		this.graph.cy.edges('[?alternative]').addClass('alternative-edge');
		this.graph.cy.edges('[!alternative]').removeClass('alternative-edge');
	}
}