/// <reference path="../cytoscape.d.ts" />

import { Injectable, EventEmitter }	from '@angular/core';

import { CssColors }				from '../css-colors';
import { TypeControl }				from '../type-control';
import { TypeSelect }				from '../type-select';
import { Service } 					from '../service';

@Injectable()
export class GraphService {
	
	/**
	 * Auto-increment ID for nodes.
	 * @member {number} GraphService#lastNodeID
	 */
	lastNodeID: number;

	/**
	 * Instance of cytoscape.
	 * Every manipulation of the graph is done with this property.
	 * @member {any} GraphService#cy
	 */
	cy: any;

	/**
	 * Constructor.
	 * @class GraphService
	 * @classdesc Service which will take care of create nodes.
	 * It will also check the correctness of the graph.
	 * i.e. check if there is no cycle, the workflow has a name, every service has a name too, etc..
	 */
	constructor() {
		this.lastNodeID = 0;
	}

	/**
	 * Add node to the graph (when clicking on the screen).
	 * Display it in the current position of the mouse when click event is triggered.
	 * @method GraphService#addNodeOnClick
	 * @param {any} event The event object cytoscape used for position purposes
	 */
	public addNodeOnClick(event: any) {
		// To get current position when we add the node
		var position = event.cyRenderedPosition;

		// Increment node ID
		this.lastNodeID++;

		// Add cytoscape element (node) and create associated Service
		this.cy.add({
			group: "nodes",
			data: {
				id: this.lastNodeID.toString(),
				service: new Service(this.lastNodeID)
			},
			renderedPosition: position
		});
		console.log("Node with id: " + this.cy.nodes()[this.cy.nodes().length - 1].data('id') + " added");
	}

	/**
	 * Add node to the graph (when clicking on the screen).
	 * Display it in the current position of the mouse when click event is triggered.
	 * But this method is called when a service fails and is not supervised.
	 * We add a tag "update" to know that it is a node created during the execution of the workflow.
	 * @method GraphService#addNodeOnClickAfterUpdate
	 * @param {any} event The event object cytoscape used for position purposes
	 */
	public addNodeOnClickAfterUpdate(event: any) {
		// To get current position when we add the node
		var position = event.cyRenderedPosition;

		// Increment node ID
		this.lastNodeID++;

		// Add cytoscape element (node) and create associated Service
		this.cy.add({
			group: "nodes",
			data: {
				id: this.lastNodeID.toString(),
				service: new Service(this.lastNodeID),
				update: true
			},
			renderedPosition: position
		});
		console.log("Node with id: " + this.cy.nodes()[this.cy.nodes().length - 1].data('id') + " added as alternative");
	}

	/**
	 * Add node to the graph.
	 * Method used when importing json to create graph.
	 * @method GraphService#addNode
	 * @param {string} id The node id
	 * @param {Service} service The Service instance of this node
	 * @param {any} position The position of the node
	 */
	public addNode(id: string, service: Service, position: any) {
		// Increment node ID
		this.lastNodeID++;
		
		// Add cytoscape element (node) and associate Service instance and specify its position
		this.cy.add({
			group: "nodes",
			data: {
				id: id,
				service: service
			},
			position: position
		});
	}

	/**
	 * Check if the graph has at least one root (basic cycle checking).
	 * @method GraphService#isGraphWithRoots
	 * @returns {boolean} true if the graph has one root or more, false otherwise
	 */
	public isGraphWithRoots() : boolean {
		if(this.cy.nodes().roots().length == 0) {
			console.error("No root on this graph");
			return false;
		}
		return true;
	}

	/**
	 * Check if the graph has cycle(s) recursively.
	 * @method GraphService#isGraphCyclic
	 * @param {any} currentNode The current node
	 * @returns {boolean} true if the graph has one or more cycles, false otherwise
	 */
	public isGraphCyclic(currentNode : any) : boolean {
		var edges = currentNode.outgoers("edge");
		var nodes = currentNode.outgoers("node");
		// Iterate over edges of the graph
		for(var i = 0; i < edges.length; i++) {
			var dst = edges[i];

			// If the current edge has not already been visited
			if(dst.data('visited') == undefined || dst.data('visited') == false) {
				// We put a label 'visited' on this edge
				dst.data('visited', true);
			}
			else {
				// If the edge has already been visited, we put a label 'cycle' on it
				dst.data('cycle', true);
				return true;
			}
		}

		// Iterate over nodes of the graph
		for(var i = 0; i < nodes.length; i++) {
			var node = nodes[i];
			// We do the same thing for the next node
			this.isGraphCyclic(node);
		}

		return this.cy.edges("[?cycle]").length != 0;
	}

	/**
	 * Check if all services have a srv field (a.k.a. a command name for each service).
	 * @method GraphService#isAllServicesWithSrv
	 * @returns {boolean} true if all services have a srv, false otherwise
	 */
	public isAllServicesWithSrv() : boolean {
		var services = this.getNodes();
		// Iterate over nodes of the graph
		for(var i = 0; i < services.length; i++) {
			if(services[i].data('service').srv.length == 0 || services[i].data('service').srv[0] == '') {
				console.error("At least one service has no srv");
				return false;
			}
		}
		return true;
	}

	/**
	 * Check if the workflow has a name.
	 * @method GraphService#isWorkflowWithName
	 * @param {string} workflowName The name of the workflow
	 * @returns {boolean} true if the workflow has a name, false otherwise
	 */
	public isWorkflowWithName(workflowName: string) : boolean {
		// Not only test workflowName == null beacuse of 2-way binding angular 2
		if(workflowName == null || workflowName == undefined || workflowName == '') {
			console.error("Workflow name required");
			return false;
		}
		return true;
	}

	/**
	 * Check if the workflow has at least one service.
	 * @method GraphService#isWorkflowWithNoService
	 * @returns {boolean} true if the workflow has at least one service, false otherwise
	 */
	public isWorkflowWithNoService() : boolean {
		// If there is no nodes in the graph
		if(this.getNodes().length == 0) {
			console.error("No services in the workflow");
			return true;
		}
		return false;
	}

	/**
	 * Check if the cytoscape extension edgehandles is enabled.
	 * Used as test for event listeners in the graph.
	 * @method GraphService#checkEdgeHandlesExtEnabled
	 */
	public checkEdgeHandlesExtEnabled() {
		return this.cy.edgehandles('option', 'enabled');
	}

	/**
	 * Get nodes of the graph.
	 * @method GraphService#getNodes
	 * @returns {any} The nodes of the graph
	 */
	public getNodes() : any {
		return this.cy.nodes();
	}

	/**
	 * Get edges of the graph.
	 * @method GraphService#getEdges
	 * @returns {any} The edges of the graph
	 */
	public getEdges() : any {
		return this.cy.edges();
	}

	/**
	 * Get nodes of the graph matching the specified selector.
	 * @method GraphService#getNodesBySelector
	 * @param {string} selector The selector
	 * @returns The nodes matching the specified selector
	 */
	public getNodesBySelector(selector : string) {
		return this.cy.nodes(selector);
	}

	/**
	 * Get a node by its id.
	 * @method GraphService#getNodeById
	 * @param {number} id The id of the node
	 * @returns {any} The node of the graph which match the id (if exist)
	 */
	public getNodeById(id : number) : any {
		return this.cy.getElementById(id);
	}

	/**
	 * Get a node by the name of its service.
	 * @method GraphService#getServiceByName
	 * @param {string} name The name of the service
	 * @returns {any} The node corresponding to the service name 'name'
	 */
	public getServiceByName(name : string) : any {
		var services = this.getNodes();
		// Warning : cytoscape filter overrides the native filter function js
		// So this.data concerns the current service in the services array
		var filtered = services.filter(function() {
			return this.data('service').name == name
		});
		return filtered[0];
	}

	/**
	 * Clear the graph by removing all elements.
	 * @method GraphService#clearGraph
	 */
	public clearGraph() {
		this.cy.elements().remove();
	}

	/**
	 * Create and return a hashmap which contains all the supervised groups in the graph.
	 * It's a part of {@link Rebranching} object.
	 * @method GraphService#getSupervised
	 * @returns {any} The hashmap which contains data about all supervised groups
	 */
	public getSupervised() : any {
		var supervised = {};
		for(var i = 0; i < this.getNodes().length; i++) {
			var service = this.getNodes()[i].data('service');
			var sup = service.sup;

			if(!isNaN(sup)) {
				this.addGroup(supervised, sup, service);
			}
		}
		return supervised;
	}

	/**
	 * Build hashmaps to build alternatives on the fly.
	 * @method GraphService#buildAlternatives
	 * @returns {any} The hashmap which contains hashmaps (src, dst, srcControl, dstControl)
	 * @see Rebranching
	 */
	public buildAlternatives() : any {
		var update = {};
		for(var i = 0; i < this.getNodes().length; i++) {
			var service = this.getNodes()[i].data('service');
			var alt = service.alt;
			var srcs = service.src;
			var dsts = service.dst;
			var srcControls = service.srcControl;
			var dstControls = service.dstControl;

			if(!isNaN(alt)) {
				update[alt] = this.getOrSetHashKey(update, alt);
				for(var j = 0; j < srcs.length; j++) {
					var src = srcs[j];
					var altSrc = src.alt;
					if(isNaN(altSrc)) {
						var updateSrc = update[alt].updateSrc;
						this.addGroup(updateSrc, src.name, service.name);
					}
				}
				for(var j = 0; j < dsts.length; j++) {
					var dst = dsts[j];
					var altDst = dst.alt;
					if(isNaN(altDst)) {
						var updateDst = update[alt].updateDst;
						this.addGroup(updateDst, dst.name, service.name);
					}
				}
				for(var j = 0; j < srcControls.length; j++) {
					var srcControl = srcControls[j];
					var altSrcControl = srcControl.alt;
					if(isNaN(altSrcControl)) {
						var updateSrcControl = update[alt].updateSrcControl;
						this.addGroup(updateSrcControl, srcControl.name, service.name);
					}
				}
				for(var j = 0; j < dstControls.length; j++) {
					var dstControl = dstControls[j];
					var altDstControl = dstControl.alt;
					if(isNaN(altDstControl)) {
						var updateDstControl = update[alt].updateDstControl;
						this.addGroup(updateDstControl, dstControl.name, service.name);
					}
				}
			}
		}
		return update;
	}

	/**
	 * Utility method to add data to a hashmap.
	 * Used in the method {@link GraphService#buildAlternatives}.
	 * @method GraphService#addGroup
	 * @private
	 * @param {any} hash The hashmap
	 * @param {any} key The key for the hashmap
	 * @param {any} service_name The value associated with the key
	 */
	private addGroup(hash : any, key : any, service_name : any) {
		if(hash[key] == undefined) {
			hash[key] = [];
		}
		hash[key].push(service_name);
	}

	/**
	 * Utility method to init hashmap containing data for rebranchings.
	 * Used in the method {@link GraphService#buildAlternatives}.
	 * @method GraphService#getOrSetHashKey
	 * @private
	 * @param {any} hash The hashmap
	 * @param {any} key The key for the hashmap
	 * @returns {any} The object contained associated to the key passed in parameter
	 */
	private getOrSetHashKey(hash : any, key : any) : any {
		if(hash[key] == undefined) {
			hash[key] = {
				updateSrc: {},
				updateDst: {},
				updateSrcControl: {},
				updateDstControl: {}
			};
		}
		return hash[key];
	}
}