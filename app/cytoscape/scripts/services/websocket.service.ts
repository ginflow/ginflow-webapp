import { Injectable } 				from '@angular/core';
import { Subject }					from 'rxjs/Subject';

import { GinflowNotifierService } 	from './ginflow-notifier.service';
import { GraphService } 			from './graph.service';
import { StyleUpdaterService } 		from './style-updater.service';
import { JsonService } 				from './json.service';

import { WorkflowStatus } 			from '../workflow-status';
import { Rebranching } 				from '../rebranching';

@Injectable()
export class WebSocketService {
	/**
	 * @member {WebSocket} WebSocketService#ws
	 * @private
	 */
	private ws: WebSocket;

	/**
	 * Subject (RxJS) is used to send data to the {@link GraphComponent}.
	 * Then the {@link GraphComponent} will subscribe to this Subject and will detect each new value sent by this subject.
	 * Used to send values taken by {@link WebSocketService#workflowStatus} attribute.
	 * @member {Subject<WorkflowStatus>} WebSocket#workflowStatusSource
	 */
	workflowStatusSource = new Subject<WorkflowStatus>();

	/*
	 * Used as value for Subject {@link WebSocketService#workflowStatusSource}.
	 * The current status of the workflow.
	 * The possible values are available in the class {@link WorkflowStatus}.
	 * @member {WorkflowStatus} WebSocketService#workflowStatus
	 */
	workflowStatus: WorkflowStatus = WorkflowStatus.ON_CREATION;

	/*
	 * Used to send values taken by {@link WebSocketService#workflowProgressionDeployment} attribute.
	 * @member {Subject<number>} WebSocketService#workflowProgressionDeploymentSource
	 */
	workflowProgressionDeploymentSource = new Subject<number>();

	/*
	 * Used to send values taken by {@link WebSocketService#workflowProgressionDescription} attribute.
	 * @member {Subject<number>} WebSocketService#workflowProgressionDescriptionSource
	 */
	workflowProgressionDescriptionSource = new Subject<number>();

	/*
	 * Used to send values taken by {@link WebSocketService#workflowProgressionResult} attribute.
	 * @member {Subject<number>} WebSocketService#workflowProgressionResultSource
	 */
	workflowProgressionResultSource = new Subject<number>();

	/*
	 * Used as value for Subject {@link WebSocketService#workflowProgressionDeploymentSource}.
	 * The current progression (converted as a purcentage later) of services deployment.
	 * @member {number} WebSocketService#workflowProgressionDeployment
	 */
	workflowProgressionDeployment: number = 0;

	/*
	 * Used as value for Subject {@link WebSocketService#workflowProgressionDescriptionSource}.
	 * The current progression (converted as a purcentage later) of services description sent.
	 * @member {number} WebSocketService#workflowProgressionDescription
	 */
	workflowProgressionDescription: number = 0;

	/*
	 * Used as value for Subject {@link WebSocketService#workflowProgressionResultSource}.
	 * The current progression (converted as a purcentage later) of services end of execution.
	 * @member {number} WebSocketService#workflowProgressionResult
	 */
	workflowProgressionResult: number = 0;

	/**
	 * Constructor.
	 * Injects an instance of {@GraphService}, {@link GinflowNotifierService}, {@link StyleUpdaterService}
	 * @class WebSocketService
	 * @classdesc Service which will take care of sending to and receive from Ginflow data.
	 * The communication between this service and Ginflow is done via WebSocket.
	 * {@link GraphService} is used to get nodes of the graph because the core instance of Cytoscape is owned by this service.
	 * {@link GinflowNotifierService} is used to update state of nodes when messages are sent from Ginflow and received by this service.
	 * {@link StyleUpdaterService} is used to show alternatives when a supervised node fails.
	 */
	constructor(private graph : GraphService,
				private ginflowNotifier : GinflowNotifierService,
				private styleUpdater : StyleUpdaterService,
				private json : JsonService) {
		this.ws = new WebSocket("ws://localhost:8025/websocket");
		// Called when the communication between this service and Ginflow is done
		this.ws.onopen = event => {
			console.log("Connection is now opened");
		}

		// Called when Ginflow send a message to this service
		this.ws.onmessage = event => {
			// The entire message sent by Ginflow
			var global_json = JSON.parse(event.data);

			/*
			 * Type of the message
			 * Possible values:
			 * 		- WORKFLOW_STARTED
			 * 		- DEPLOYMENT_SUCCESS
			 * 		- DESCRIPTION_SENT
			 * 		- RESULT
			 * 		- SERVICE_FAILURE
			 * 		- WORKFLOW_TERMINATED
			 */
			var type = global_json.type;
			var pourcentage;

			// It does not have received the message WORKFLOW_TERMINATED yet
			if(type != undefined && type != "WORKFLOW_TERMINATED") {
				// Get the text of the message
				var message = global_json.message;

				// Parse this text to JSON
				var json = JSON.parse(message);

				// Ginflow that the workflow started
				if(type == "WORKFLOW_STARTED") {
					this.workflowStatus = WorkflowStatus.STARTED;

					// Send to the GraphComponent the status of the workflow
					this.workflowStatusSource.next(this.workflowStatus);
					console.log("Workflow started");
				}
				// Ginflow send that a service has terminated
				else if(type == "RESULT") {
					// Get the current service
					var currentService = this.ginflowNotifier.getCurrentExecutedService(json);

					/*
					 * Test used for adpativeness.
					 * Because, in several cases RESULT message can be send more than once for a same service
					 */
					if(currentService.data('called') == undefined || !currentService.data('called')) {
						currentService.data('called', true);
						/*
						 * Increment progression of executed services
						 * If it's the first RESULT message for this service
						 */
						this.workflowProgressionResult ++;
					}

					// Update css for this service
					this.workflowProgressionResult = this.ginflowNotifier.updateServiceStatus(json, this.workflowProgressionResult);

					// Convert the progression number into purcentage
					pourcentage = Math.floor((this.workflowProgressionResult / this.graph.getNodes().length) * 100);

					// Send to the GraphComponent the new purcentage of executed services
					this.workflowProgressionResultSource.next(pourcentage);

					// Get the exit code of the current service
					var exit = this.ginflowNotifier.services_map[currentService.data('service').name].exit;

					// Get its supervised group
					var supervised = currentService.data('service').sup;

					/*
					 * Show alternatives which belongs to the same group as the current service.
					 * If the status code > 0 and if the current service has supervised group.
					 */
					if(exit != '0' && !isNaN(supervised)) {
						this.styleUpdater.showAlternatives(supervised);
					}

					if(exit != '0' && (isNaN(supervised) || supervised == undefined || supervised == "")) {
						this.workflowStatus = WorkflowStatus.ON_UPDATE;
					}

				}

				// Ginflow send that a service description is sent
				else if(type == "DESCRIPTION_SENT") {
					// Update css for this service
					this.ginflowNotifier.notifyServiceStarted(json);

					// Increment progression of services description sent
					this.workflowProgressionDescription ++;

					// Convert the progression number into purcentage
					pourcentage = Math.floor((this.workflowProgressionDescription / this.graph.getNodes().length) * 100);

					// Send to the GraphComponent the new purcentage of services description sent
					this.workflowProgressionDescriptionSource.next(pourcentage);
				}

				// Ginflow send that a service is deployed
				else if(type == "DEPLOYMENT_SUCCESS") {
					// Update css for this service
					this.ginflowNotifier.notifyServiceDeployed(json);

					// Increment progression of services deployment
					this.workflowProgressionDeployment ++;

					// Convert the progression number into purcentage
					pourcentage = Math.floor((this.workflowProgressionDeployment / this.graph.getNodes().length) * 100);

					// Send to the GraphComponent the new purcentage of services deployment
					this.workflowProgressionDeploymentSource.next(pourcentage);

					// Update the status of the workflow if it's the first service which is deployed
					if(this.workflowStatus == WorkflowStatus.STARTED) {
						// Set workflow status 'ON_EXECUTION'
						this.workflowStatus = WorkflowStatus.ON_EXECUTION;

						// Send to the GraphComponent the new status of the workflow
						this.workflowStatusSource.next(this.workflowStatus);
					}
				}

				// Ginflow send that a service has crashed (not error but a real crash)
				else if(type == "SERVICE_FAILURE") {
					// Get the current service
					var currentService = this.ginflowNotifier.getCurrentExecutedService(json);

					// Decrement progression of executed services
					this.workflowProgressionResult --;

					// Set the flag called to false
					currentService.data('called', false);

					// Convert the progression number into purcentage
					pourcentage = Math.floor((this.workflowProgressionResult / this.graph.getNodes().length) * 100);

					// Send to the GraphComponent the new purcentage of executed services
					this.workflowProgressionResultSource.next(pourcentage);

					if(this.workflowStatus == WorkflowStatus.ON_EXECUTION) {
						// Set status of the workflow to 'FAILED' if one service has error
						this.workflowStatus = WorkflowStatus.FAILED;

						// Send to the GraphComponent the new status of the workflow
						this.workflowStatusSource.next(this.workflowStatus);
					}

					// Update css for this service
					this.ginflowNotifier.notifyServiceFailed(json);
				}
			}

			// Ginflow send that the workflow is terminated
			if(type == "WORKFLOW_TERMINATED") {
				// No services have failed
				if(this.workflowStatus != WorkflowStatus.FAILED) {
					// Set status of the workflow to 'FINISHED'
					this.workflowStatus = WorkflowStatus.FINISHED;

					// Send to the GraphComponent the new status of the workflow
					this.workflowStatusSource.next(this.workflowStatus);

					// Send to the GraphComponent the new purcentage of executed services
					this.workflowProgressionResultSource.next(100);
				}
				console.log("Workflow is finished");

				// Close connection with Ginflow
				this.ws.close();
				console.log("Connection to Websocket closed");
			}
		}
	}

	/**
	 * Main method used to send and receive data to/from Ginflow.
	 * Uses native class WebSocket JS, for more informations : {@link https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API}
	 * @param {any} workflow the data concerning the workflow (i.e. name + services)
	 * @method WebSocketService#sendData
	 */
	public sendData(workflow: any) {
		if(this.ws.readyState == 1) { // OPEN
			if(this.workflowStatus == WorkflowStatus.ON_CREATION) {

				var workflow_stringified = JSON.stringify({"type": "WORKFLOW_CREATION", "message": workflow});
				console.log(workflow_stringified);
				// console.log(JSON.stringify(workflow));

				// this.ws.send(JSON.stringify(workflow));

				// Send firstly the workflow to Ginflow
				this.ws.send(workflow_stringified);
			}
			else if(this.workflowStatus == WorkflowStatus.ON_UPDATE) {
				var non_tagged_services = this.graph.cy.nodes("[!update]");
				var tagged_services = this.graph.cy.nodes("[?update]");

				var supervised = this.graph.getSupervised();
				var update = this.graph.buildAlternatives();

				var rebranchings = new Array<Rebranching>();

				for(var key in supervised) {
					var sups = [];
					for(var i = 0; i < supervised[key].length; i++) {
						var sup = supervised[key][i].name;
						sups.push(sup);
					}

					var rebranching = new Rebranching(sups, update[key].updateSrc, update[key].updateDst, update[key].updateSrcControl, update[key].updateDstControl);
					rebranchings.push(rebranching);
				}

				var services = [];
				for(var i = 0; i < tagged_services.length; i++) {
					var service = tagged_services[i].data('service');
					services.push(service);
				}

				var updated_json = this.json.cytoscapeGraphUpdatedToGinflowWorkspaceJson("updated-workflow", tagged_services);

				updated_json["rebranchings"] = rebranchings;

				console.log(updated_json);


				var workflow_stringified = JSON.stringify({"type": "WORKFLOW_UPDATE", "message": updated_json});

				console.log(workflow_stringified);
				this.ws.send(workflow_stringified);
			}
		}
	}
}
