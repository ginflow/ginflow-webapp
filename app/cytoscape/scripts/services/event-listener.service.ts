import { Injectable, EventEmitter }	from '@angular/core';

import { GraphService }				from './graph.service';
import { StyleUpdaterService }		from './style-updater.service';
import { GinflowNotifierService }	from './ginflow-notifier.service';
import { WebSocketService }			from './websocket.service';

import { TypeSelect }				from '../type-select';
import { TypeControl }				from '../type-control';
import { Service }					from '../service';
import { WorkflowStatus }			from '../workflow-status';

@Injectable()
export class EventListenerService {

	/**
	 * Constructor.
	 * Inject instance of {@link GraphService}, {@link StyleUpdaterService}, {@link GinflowNotifierService}.
	 * @class EventListenerService
	 * @classdesc This class is taking care of every user interaction in the graph.
	 * 1) Click on node, on edge or on empty location
	 * 2) Right click on node or edge
	 * 3) Drag & Drop between two nodes
	 * In this class an instance of {@link StyleUpdaterService} is used to add or remove CSS class for elements (nodes/edges).
	 * Classes CSS used are defined in the class {@link CytoscapeInitialisationService}.
	 * {@link GraphService} is used to handle these events because this class owns the core instance of Cytoscape ({@link GraphService#cy}).
	 * {@link GinflowNotifierService} is used to get the result of the execution of a selected node in the graph.
	 * {@link WebSocketService} is used to get the status of the workflow.
	 * 
	 * @param {GraphService} graphService An instance of {@link GraphService}
	 * @param {StyleUpdaterService} styleUpdater An instance of {@link StyleUpdaterService}
	 * @param {GinflowNotifierService} ginflowNotifier An instance of {@link GinflowNotifierService}
	 * @param {WebSocketService} webSocketService An instance of {@link WebSocketService}
	 * 
	 * @see GraphService
	 * @see StyleUpdaterService
	 * @see GinflowNotifierService
	 * @see WebSocketService
	 */
	constructor(private graphService : GraphService,
				private styleUpdater : StyleUpdaterService,
				private ginflowNotifier : GinflowNotifierService,
				private webSocketService : WebSocketService
	) {}

	/**
	 * Event handler listening for a click in an empty zone.
	 * Create a new node on the location of the mouse pointer.
	 * @method EventListenerService#clickToCreateNodeRoutine
	 */
	public clickToCreateNodeRoutine() {
		// Click performed
		this.graphService.cy.on('click', event => {
			// If the extension is not enabled we do nothing
			if(!this.graphService.checkEdgeHandlesExtEnabled()) {
				return;
			}
			
			// In empty location on the graph
			if(event.cyTarget.data == null) {
				console.log(this.webSocketService.workflowStatus);
				if(this.webSocketService.workflowStatus == WorkflowStatus.ON_CREATION) {
					// Add node
					this.graphService.addNodeOnClick(event);
					console.log("Add node during creation");
				}
				else if(this.webSocketService.workflowStatus == WorkflowStatus.ON_UPDATE) {
					this.graphService.addNodeOnClickAfterUpdate(event);
					console.log("Add node during update");
				}
			}
		});
	}

	/**
	 * Event handler listening for a click in a node.
	 * Select the clicked node and apply some css and display some informations.
	 * @method EventListenerService#clickToSelectNodeRoutine
	 * @param {EventEmitter} eventEmitter An event which will be sent to the parent component ({@link AppComponent})
	 */
	public clickToSelectNodeRoutine(eventEmitter : EventEmitter<{}>) {
		// Click on node
		this.graphService.cy.on('click', 'node', event => {
			// Check if it's a zone with a node where the click was performed
			if(event.cyTarget.data != null) {

				// Get the id of the clicked node
				var id = event.cyTarget.data('id');

				// If the node is already selected then it's a deselection
				if(this.graphService.getNodeById(id).hasClass('selected-node')) {

					this.styleUpdater.hideAlternatives();

					// We update css style accordingly to the current status of the node
					this.graphService.getNodeById(id).removeClass('selected-node');
					
					// If the service is done
					if(this.graphService.getNodeById(id).hasClass('selected-validated-node')) {
						this.graphService.getNodeById(id).removeClass('selected-validated-node');
					}

					// If the service has failed
					else if(this.graphService.getNodeById(id).hasClass('selected-failed-node')) {
						this.graphService.getNodeById(id).removeClass('selected-failed-node');
					}

					// If the service has its description sent
					else if(this.graphService.getNodeById(id).hasClass('selected-ready-node')) {
						this.graphService.getNodeById(id).removeClass('selected-ready-node');
					}

					// If the service is deployed
					else if(this.graphService.getNodeById(id).hasClass('selected-deployed-node')) {
						this.graphService.getNodeById(id).removeClass('selected-deployed-node');
					}

					// Send to GraphComponent and AppComponent that it is a deselection
					eventEmitter.emit({
						value: {
							type: TypeSelect.NodeDeselection
						}
					});
				}

				else {
					var previous_service_selected = this.graphService.cy.nodes('.selected-node');

					// Remove all css style concerning selected nodes
					this.styleUpdater.clearStyleAllSelectedNodes();

					// We update css style accordingly to the current status of the node
					this.graphService.getNodeById(id).addClass('selected-node');

					// If the service is done
					if(this.graphService.getNodeById(id).hasClass('validated-node')) {
						// If another node is already selected and is validated we clear selected selector for this node
						this.styleUpdater.clearStyleValidatedSelectedNodes();
						// Applying CSS class 'selected-validated-node' (Node colored in dark-green)
						this.graphService.getNodeById(id).addClass('selected-validated-node');
					}

					// If the service has failed
					else if(this.graphService.getNodeById(id).hasClass('failed-node')) {
						// If another node is already selected and is failed we clear selected selector for this node
						this.styleUpdater.clearStyleFailedSelectedNodes();
						// Applying CSS class 'selected-failed-node' (Node colored in dark-red)
						this.graphService.getNodeById(id).addClass('selected-failed-node');
					}

					// If the service has its description sent
					else if(this.graphService.getNodeById(id).hasClass('ready-node')) {
						// If another node is already selected and has its description sent we clear selected selector for this node
						this.styleUpdater.clearStyleReadySelectedNodes();
						// Applying CSS class 'selected-ready-node' (Node colored in dark-orange)
						this.graphService.getNodeById(id).addClass('selected-ready-node');
					}

					// If the service is deployed
					else if(this.graphService.getNodeById(id).hasClass('deployed-node')) {
						// If another node is already selected and has is deployed we clear selected selector for this node
						this.styleUpdater.clearStyleDeployedSelectedNodes();
						// Applying CSS class 'selected-deployed-node' (Node colored in dark-purple)
						this.graphService.getNodeById(id).addClass('selected-deployed-node');
					}

					// Send to GraphComponent and AppComponent that it is a selection
					// Send the service, its id and its result
					eventEmitter.emit({
						value: {
							id : event.cyTarget.data('id'),
							service: event.cyTarget.data('service'),
							type: TypeSelect.NodeSelection,
							result: this.ginflowNotifier.services_map[event.cyTarget.data('service').name]
						}
					});

					// Clear all style of selected edges in the graph
					this.styleUpdater.clearStyleSelectedEdges();


					// Get the supervised group of this node
					var sup = this.graphService.getNodeById(id).data('service').sup;
					var alt = this.graphService.getNodeById(id).data('service').alt;
					// If this node has a well-defined supervised group
					if(!isNaN(sup)) {
						// Show alternatives which belong to the supervised group of the selected node
						this.styleUpdater.showAlternatives(sup);
						var elements = this.graphService.cy.elements().remove();
						elements.restore();
					}
					else if(!isNaN(alt)) {
						// Show alternatives which belong to the alternative group of the selected node
						this.styleUpdater.showAlternatives(alt);
					}
					else {
						var supervised;
						if(previous_service_selected.length != 0) {
							supervised = previous_service_selected.data('service').sup;
							if(!isNaN(supervised)) {
								this.styleUpdater.hideAlternative(supervised);
							}
							else {
								this.styleUpdater.hideAlternatives();
							}
						}
						else {
							this.styleUpdater.hideAlternatives();
						}
					}

				}
			}
		});
	}

	/**
	 * Event handler listening for a click in an edge.
	 * Select the clicked edge and apply some css and display some informations.
	 * @method EventListenerService#clickToSelectEdgeRoutine
	 * @param {EventEmitter} eventEmitter An event which will be sent to the parent component ({@link AppComponent})
	 */
	public clickToSelectEdgeRoutine(eventEmitter : EventEmitter<{}>) {
		// Click on edge
		this.graphService.cy.on('click', 'edge', event => {
			// Check if it's a zone with an edge where the click was performed
			if(event.cyTarget.data != null) {
				// Get the id of the clicked edge
				var id = event.cyTarget.data('id');

				// If the edge is already selected then it's a deselection
				if(this.graphService.getNodeById(id).hasClass('selected-edge')) {
					this.styleUpdater.hideAlternatives();
					this.graphService.getNodeById(id).removeClass('selected-edge');
					
					// Send to GraphComponent and AppComponent that it is a deselection
					eventEmitter.emit({
						value: {
							type: TypeSelect.EdgeDeselection
						}
					});
				}

				// Selection
				else {
					// Clear all style of selected edges in the graph
					this.styleUpdater.clearStyleSelectedEdges();

					// Apply the css class 'selected-edge' to the clicked edge 
					this.graphService.getNodeById(id).addClass('selected-edge');
					
					// Send to GraphComponent and AppComponent that it is a selection
					// Send the source, the target and the graph instance
					eventEmitter.emit({
						value: {
							source : event.cyTarget.data('source'),
							target : event.cyTarget.data('target'),
							type_control: this.graphService.cy.edges('.selected-edge').data('type_control'),
							type: TypeSelect.EdgeSelection
						}
					});

					// Clear all style of selected nodes in the graph
					this.styleUpdater.clearStyleAllSelectedNodes();
				}
			}
		});
	}

	/**
	 * Event handler listening for a right-click in a node.
	 * Delete the clicked node.
	 * @method EventListenerService#contextClickToDeleteNodeRoutine
	 * @param {EventEmitter} eventEmitter An event which will be sent to the parent component ({@link AppComponent})
	 */
	public contextClickToDeleteNodeRoutine(eventEmitter : EventEmitter<{}>) {
		// Right click on node
		this.graphService.cy.on('cxttap', 'node', event => {
			// Do nothing if the extension is disable
			if(!this.graphService.checkEdgeHandlesExtEnabled()) {
				return;
			}

			// Right click is performed in a node
			if(event.cyTarget.data != null) {
				// Get the id of the node which has been right-clicked
				var id = event.cyTarget.id();
				
				// If the node has been selected just before this right-click
				if(this.graphService.getNodeById(id).hasClass('selected-node')) {
					// Removing CSS class 'selected-node'
					this.graphService.getNodeById(id).removeClass('selected-node');

					// Send to GraphComponent and AppComponent that it is a deselection
					eventEmitter.emit({
						value: {
							type: TypeSelect.NodeDeselection
						}
					});
				}
				else {
					// It exists a node which is selected
					if(this.graphService.cy.nodes('.selected-node') != null) {
						// Send to GraphComponent and AppComponent that it is a deselection
						// Send the id and the instance of Service of the node which is selected
						eventEmitter.emit({
							value: {
								id: this.graphService.cy.nodes('.selected-node').data('id'),
								service: this.graphService.cy.nodes('.selected-node').data('service'),
								type: TypeSelect.NodeDeselection
							}
						});
					}

					// No node is selected
					else {
						// We only send to GraphComponent and AppComponent that it is a deselection
						eventEmitter.emit({
							value: {
								type: TypeSelect.NodeDeselection
							}
						});
					}
				}
				// Remove node from the graph (and implicitly its connected edges)
				this.graphService.cy.remove(event.cyTarget);

				// Deselection of all selected edges
				this.styleUpdater.clearStyleSelectedEdges();

				// Deselection of all selected nodes
				this.styleUpdater.clearStyleSelectedNodes();

				console.log("Node with id: " + id + " removed");
			}
		});
	}

	/**
	 * Event handler listening for a right-click in an edge.
	 * Delete the clicked edge, also delete nodes linked.
	 * @method EventListenerService#contextClickToDeleteEdgeRoutine
	 * @param {EventEmitter} eventEmitter An event which will be sent to the parent component ({@link AppComponent})
	 */	
	public contextClickToDeleteEdgeRoutine(eventEmitter : EventEmitter<{}>) {
		// Right click on edge
		this.graphService.cy.on('cxttap', 'edge', event => {
			// Do nothing if the extension is disable
			if(!this.graphService.checkEdgeHandlesExtEnabled()) {
				return;
			}

			// Right click is performed in an edge
			if(event.cyTarget.data != null) {
				// Get the id of the edge which has been right-clicked
				var id = event.cyTarget.id();
				
				// If the edge has been selected just before this right-click
				if(this.graphService.getNodeById(id).hasClass('selected-edge')) {
					// Removing CSS class 'selected-edge'
					this.graphService.getNodeById(id).removeClass('selected-edge');
					
					// Send to GraphComponent and AppComponent that it is a deselection
					eventEmitter.emit({
						value: {
							type: TypeSelect.EdgeDeselection
						}
					});
				}

				else {
					// It exists an edge which is selected
					if(this.graphService.cy.edges('.selected-edge') != null) {
						// Send to GraphComponent and AppComponent that it is a deselection
						// Send the source, the target, the type of edge of the edge which is selected
						eventEmitter.emit({
							value: {
								source: this.graphService.cy.edges('.selected-edge').data('source'),
								target: this.graphService.cy.edges('.selected-edge').data('target'),
								type_control: this.graphService.cy.edges('.selected-edge').data('type_control'),
								type: TypeSelect.EdgeDeselection
							}
						});
					}

					// No node is selected
					else {
						// We only send to GraphComponent and AppComponent that it is a deselection
						eventEmitter.emit({
							value: {
								type: TypeSelect.EdgeDeselection
							}
						});
					}
				}
				// Remove edge from the graph
				this.graphService.cy.remove(event.cyTarget);

				// Deselection of all selected edges
				this.styleUpdater.clearStyleSelectedEdges();

				// Deselection of all selected nodes
				this.styleUpdater.clearStyleSelectedNodes();

				console.log("Edge with id: " + id + " removed");
			}
		});
	}

	/**
	 * Event handler listening for a drag and drop between two nodes.
	 * Create edge between two nodes.
	 * @method EventListenerService#dragAndDropToCreateEdgeRoutine
	 */
	public dragAndDropToCreateEdgeRoutine() {
		// Drag & Drop between two nodes is completed
		this.graphService.cy.on('cyedgehandles.complete', event => {
			// Do nothing if the extension is disable
			if(!this.graphService.checkEdgeHandlesExtEnabled()) {
				return;
			}
			console.log("Edge with id: " + this.graphService.cy.edges()[this.graphService.cy.edges().length - 1].id() + " added");

			// When we create an edge we set by default the type of the created edge on 'Data'
			this.graphService.cy.edges()[this.graphService.cy.edges().length - 1].data('type_control', TypeControl.Data);
		});
	}
}