import { Injectable }		from '@angular/core';

import { GraphService }		from './graph.service';

import { GinflowResult } 	from '../ginflow-result';
import { WorkflowStatus } 	from '../workflow-status';

@Injectable()
export class GinflowNotifierService {
	/*
	 * Map 	-> Key: name of the service
	 * 		-> Value: A GinflowResult instance
	 * @member {any} GinflowNotifierService#services_map
	 */
	public services_map: any = {};

	/**
	 * Constructor.
	 * Injects an instance of {@link GraphService}.
	 * @class GinflowNotifierService
	 * @classdesc Service which will take care of changing css during the execution of the workflow.
	 * Build also a GinflowResult which contains information about service execution.
	 * i.e. Name, output, errors and exit code of the service.
	 * {@link GraphService} is used because this class owns the core instance of Cytoscape ({@link GraphService#cy}).
	 * 
	 * @param {GraphService} graph An instance of {@link GraphService}
	 * 
	 * @see GraphService
	 */
	constructor(private graph : GraphService) {}

	/**
	 * Update status of service during the execution.
	 * Update css style and retrieve the result of the execution.
	 * @method GinflowNotifierService#updateServiceStatus
	 * @param {any} service The service to update
	 * @param {number} workflowProgression The current progression in percent of the workflow
	 * @returns {number} The new progression in percent of the workflow
	 */
	public updateServiceStatus(service : any, workflowProgression: number) : number {
		// Get the current service
		var currentService = this.getCurrentExecutedService(service);

		// Get its name
		var name = service.name[0];

		// res is defined when the status code of the service is 0 (everything is ok)
		if(service.res != undefined) {
			var exit_status = service.res[0].transfer[0].exit[0];
			var output = service.res[0].transfer[1].out[0];
			var error = service.res[0].transfer[2].err[0];
		}

		// res is not defined, so the status code of the service is > 0 (command not found for example)
		else {
			// Normal case
			if(service.result[0].transfer[0].out != undefined) {
				var output = service.result[0].transfer[0].out[0];
				var error = service.result[0].transfer[1].err[0];
				var exit_status = service.result[0].transfer[2].exit[0];
			}
			// This case appears when there is adaptiveness on this service
			else {
				var exit_status = service.result[0].transfer[0].exit[0];
				var output = service.result[0].transfer[1].out[0];
				var error = service.result[0].transfer[2].err[0];
			}
		}

		// If there is error on the service (but has been executed correctly on GinFlow)
		if(exit_status != 0) {
			// We set this service in red
			currentService.removeClass("ready-node");
			currentService.addClass("failed-node");
		}
		// No error on service
		else {
			// We set this service in green
			currentService.removeClass("ready-node");
			currentService.addClass("validated-node");
		}
		// Add item on the map (key = name) (value = new GinflowResult(name, exit_status, output, error))
		this.services_map[name] = new GinflowResult(name, exit_status, output, error);
		return workflowProgression;
	}

	/**
	 * Update css style when the execution of the service is starting.
	 * @method GinflowNotifierService#notifyServiceStarted
	 * @param {any} service The service to update
	 */
	public notifyServiceStarted(service : any) {
		var currentService = this.getCurrentExecutedService(service);
		currentService.removeClass("deployed-node");
		currentService.addClass("ready-node");
	}

	/**
	 * Update css style when the service is deployed.
	 * @method GinflowNotifierService#notifyServiceDeployed
	 * @param {any} service The service to update
	 */
	public notifyServiceDeployed(service : any) {
		var currentService = this.getCurrentExecutedServiceById(service);
		currentService.addClass("deployed-node");
	}

	/**
	 * Update css style when the service crash (interruption of the service).
	 * @method GinflowNotifierService#notifyServiceFailed
	 * @param {any} service The service to update
	 */
	public notifyServiceFailed(service : any) {
		var currentService = this.getCurrentExecutedServiceById(service);
		if(currentService.hasClass("ready-node")) {
			currentService.removeClass("ready-node");
		}
		if(currentService.hasClass("deployed-node")) {
			currentService.removeClass("deployed-node");
		}
	}

	/**
	 * Get the current executed service given its id.
	 * @method GinflowNotifierService#getCurrentExecutedServiceById
	 * @param {string} id The id of the executed service
	 * @returns {Cy.Collection} The executed service (unique)
	 */
	private getCurrentExecutedServiceById(id: string) : Cy.Collection {
		var services = this.graph.getNodes();
		// Warning : cytoscape filter overrides the native filter function js
		// So this.data concerns the current service in the services array
		var filtered = services.filter(function() {
			return this.data('service').id == id
		});
		return filtered[0];
	}

	/**
	 * Get the current executed service given the instance of {@link Service}.
	 * @method GinflowNotifierService#getCurrentExecutedService
	 * @param {any} service The instance of service
	 * @returns {Cy.Collection} The executed service (unique)
	 */
	public getCurrentExecutedService(service: any) : Cy.Collection {
		var services = this.graph.getNodes();
		var filtered = services.filter(function() {
			return this.data('service').name == service.name[0]
		});
		return filtered[0];
	}
}
