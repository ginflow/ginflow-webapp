import { Component, AfterViewInit, Output, EventEmitter, OnChanges, Input } from '@angular/core';
import { CORE_DIRECTIVES }													from '@angular/common';
import { Subscription }														from 'rxjs/Subscription';
import { PROGRESSBAR_DIRECTIVES }											from 'ng2-bootstrap/ng2-bootstrap';

import { GraphService }						from '../services/graph.service';
import { EventListenerService }				from '../services/event-listener.service';
import { CytoscapeInitialisationService }	from '../services/cytoscape-initialisation.service';
import { WebSocketService }					from '../services/websocket.service';
import { StyleUpdaterService }				from '../services/style-updater.service';
import { DependenciesUpdaterService }		from '../services/dependencies-updater.service';
import { GinflowNotifierService }			from '../services/ginflow-notifier.service';
import { JsonService }						from '../services/json.service';

import { TypeSelect }						from '../type-select';
import { TypeControl }						from '../type-control';
import { Service } 							from '../service';
import { WorkflowStatus } 					from '../workflow-status';

const maxPourcentage: number = 100;

@Component({
	selector: 'cytoscape-graph',
	templateUrl: 'app/cytoscape/views/graph.component.html',
	styleUrls: ['app/cytoscape/styles/graph.component.css'],
	providers: [GraphService,
				EventListenerService,
				CytoscapeInitialisationService,
				WebSocketService,
				StyleUpdaterService,
				DependenciesUpdaterService,
				GinflowNotifierService,
				JsonService],
	directives: [PROGRESSBAR_DIRECTIVES, CORE_DIRECTIVES]
})
export class GraphComponent implements OnChanges {
	/**
	 * DOM Element which contains the graph.
	 * @member {HTMLElement} Components.GraphComponent#container
	 */
	private container: HTMLElement;

	/**
	 * Elements.
	 * @member {Cy.ElementDefinition[]} Components.GraphComponent#elements
	 */
	private elements: Cy.ElementDefinition[];

	/**
	 * Layout.
	 * @member {Cy.LayoutOptions} Components.GraphComponent#layout
	 */
	private layout: Cy.LayoutOptions;

	/**
	 * Style of elements (selectors).
	 * @member {Cy.Stylesheet[]} Components.GraphComponent#style
	 */
	private style: Cy.Stylesheet[];

	/**
	 * Boolean to display or not text area for JSON export.
	 * @member {boolean} Components.GraphComponent#exportJSON
	 */
	private exportJSON: boolean = false;

	/**
	 * Boolean to display or not text area for JSON import.
	 * @member {boolean} Components.GraphComponent#hideImportJSON
	 */
	private hideImportJSON: boolean = false;

	/**
	 * Boolean to display or not text area for JSON import.
	 * @member {boolean} Components.GraphComponent#importJSON
	 */
	private importJSON: boolean = false;

	/**
	 * Boolean used to force the export to JSON only once (except if errors occurs).
	 * @member {boolean} Components.GraphComponent#wasClickedOnce
	 */
	private wasClickedOnce: boolean = false;
	
	/**
	 * Purcentage for services deployed progressbar.
	 * @member {number} Components.GraphComponent#workflowPourcentageDeployment
	 */
	private workflowPourcentageDeployment: number = 0;

	/**
	 * Purcentage for services description sent progressbar.
	 * @member {number} Components.GraphComponent#workflowPourcentageDescription
	 */
	private workflowPourcentageDescription: number = 0;

	/**
	 * Purcentage for services result progressbar.
	 * @member {number} Components.GraphComponent#workflowPourcentageResult
	 */
	private workflowPourcentageResult: number = 0;

	/**
	 * Current status of the workflow.
	 * @member {WorkflowStatus} Components.GraphComponent#workflowStatus
	 */
	private workflowStatus: WorkflowStatus;

	/**
	 * Subscriber which listens for changes of workflowStatus.
	 * @member {Subscription} Components.GraphComponent#subscription
	 */
	private subscription: Subscription;

	/**
	 * Subscriber which listens for changes of workflowPourcentageDeployment.
	 * @member {Subscription} Components.GraphComponent#subscriptionProgressionDeployment
	 */
	private subscriptionProgressionDeployment: Subscription;

	/**
	 * Subscriber which listens for changes of workflowPourcentageDescription.
	 * @member {Subscription} Components.GraphComponent#subscriptionProgressionDescription
	 */
	private subscriptionProgressionDescription: Subscription;

	/**
	 * Subscriber which listens for changes of workflowPourcentageResult.
	 * @member {Subscription} Components.GraphComponent#subscriptionProgressionResult
	 */
	private subscriptionProgressionResult: Subscription;


	/*
	 * OUTPUTS
	 */

	/**
	 * Event sent to {@link Components.AppComponent} when selecting or deselecting element of the graph.
	 * @member {EventEmitter} Components.GraphComponent#select
	 */
	@Output()
	select = new EventEmitter();

	/**
	 * Event sent to {@link Components.AppComponent} when clicking on launch button.
	 * @member {EventEmitter} Components.GraphComponent#launch
	 */
	@Output()
	launch = new EventEmitter();

	/**
	 * Event sent to {@link Components.AppComponent} when the workflow execution ending.
	 * @member {EventEmitter} Components.GraphComponent#end
	 */
	@Output()
	end = new EventEmitter();

	/**
	 * Event sent to {@link Components.AppComponent} when a click on toggle button create/import graph.
	 * @member {EventEmitter} Components.GraphComponent#toggle
	 */
	@Output()
	toggle = new EventEmitter();

	/**
	 * Event sent to {@link Components.AppComponent} when clicking on export to JSON button.
	 * @member {EventEmitter} Components.GraphComponent#exports
	 */
	@Output()
	exports = new EventEmitter();

	/*
	 * INPUTS
	 */

	/**
	 * Data received from {@link Components.AppComponent}, represents the srv of the current service selected.
	 * @member {string} Components.GraphComponent#currentSrv
	 */
	@Input()
	currentSrv: string = '';

	/**
	 * Data received from {@link Components.AppComponent}, represents the alternative group of the current service selected.
	 * @member {number} Components.GraphComponent#currentAlt
	 */
	@Input()
	currentAlt: number = undefined;

	/**
	 * Data received from {@link Components.AppComponent}, represents the supervised group of the current service selected.
	 * @member {number} Components.GraphComponent#currentSup
	 */
	@Input()
	currentSup: number = undefined;

	/**
	 * Data received from {@link Components.AppComponent}, represents the current type of egde selected.
	 * Possible values on {@link Enums.TypeControl}.
	 * @member {any} Components.GraphComponent#currentTypeControl
	 */
	@Input()
	currentTypeControl: any = {};

	/**
	 * Data received from {@link Components.AppComponent}, represents the current edge selected.
	 * @member {any} Components.GraphComponent#currentEdge
	 */
	@Input()
	currentEdge: any = {};

	/**
	 * Data received from {@link Components.AppComponent}, represents the name of the workflow.
	 * @member {string} Components.GraphComponent#workflowName
	 */
	@Input()
	workflowName: string = '';

	/**
	 * Data received from {@link Components.AppComponent}, represents the JSON object when submitting the form to import JSON to graph.
	 * @member {any} Components.GraphComponent#importData
	 */
	@Input()
	importData: any = {};

	/**
	 * Data received from {@link Components.AppComponent}, represents the JSON object contained in the text area JSON preview.
	 * @member {string} Components.GraphComponent#exportData
	 */
	@Input()
	exportData: any = {};


	/**
	 * Constructor.
	 * @class Components.GraphComponent
	 * @classdesc Angular Component containing Cytoscape graph.
	 * Inject services on object creation.
	 * @param {GraphService} graph
	 * @param {EventListenerService} eventListenerService
	 * @param {CytoscapeInitialisationService} cytoscapeInitialisationService
	 * @param {WebSocketService} webSocketService
	 * @param {StyleUpdaterService} styleUpdater
	 * @param {DependenciesUpdaterService} dependenciesUpdater
	 * @param {GinflowNotifierService} ginflowNotifier
	 * @param {JsonService} json
	 */
	constructor(private graph: GraphService, 
				private eventListenerService : EventListenerService, 
				private cytoscapeInitialisationService : CytoscapeInitialisationService,
				private webSocketService : WebSocketService,
				private styleUpdater : StyleUpdaterService,
				private dependenciesUpdater : DependenciesUpdaterService,
				private ginflowNotifier : GinflowNotifierService,
				private json : JsonService)
	{
		// Initialize workflow status on graph creation
		this.workflowStatus = WorkflowStatus.ON_CREATION;

		/*
		 * Subscribe to value workflowStatus.
		 * When this value will change, this part of code will be called
		 */
		this.subscription = webSocketService.workflowStatusSource.subscribe((value) => {
			this.workflowStatus = value; 
		});
		
		/*
		 * Subscribe to value workflowPourcentageDeployment.
		 * When this value will change, this part of code will be called
		 */
		this.subscriptionProgressionDeployment = webSocketService.workflowProgressionDeploymentSource.subscribe((value) => {
			this.workflowPourcentageDeployment = value; 
		});
		
		/*
		 * Subscribe to value workflowPourcentageDescription.
		 * When this value will change, this part of code will be called
		 */
		this.subscriptionProgressionDescription = webSocketService.workflowProgressionDescriptionSource.subscribe((value) => {
			this.workflowPourcentageDescription = value; 
		});

		/*
		 * Subscribe to value workflowPourcentageResult.
		 * When this value will change, this part of code will be called
		 */
		this.subscriptionProgressionResult = webSocketService.workflowProgressionResultSource.subscribe((value) => {
			this.workflowPourcentageResult = value; 
		});

	}

	/**
	 * Function called when DOM is ready.
	 * All code concerning cytoscape initialisation must be put here.
	 * @method Components.GraphComponent#ngAfterViewInit
	 */
	ngAfterViewInit() {

		/**
		 * Create cytoscape instance
		 */
		this.graph.cy = cytoscape({
			container: this.cytoscapeInitialisationService.initContainer(),
			elements: this.cytoscapeInitialisationService.initElements(),
			style: this.cytoscapeInitialisationService.initStyleSheet(),
			layout: this.cytoscapeInitialisationService.initLayout()
		});

		/**
		 * Initialize edgehandles plugin (cytoscape)
		 */
		this.graph.cy.edgehandles({
			toggleOffOnLeave: true
		});

		/**
		 * Initialize panzoom plugin
		 */
		this.graph.cy.panzoom({});
		
		// Update dimensions of container
		this.graph.cy.resize();

		// Event listeners
		this.eventListenerService.clickToCreateNodeRoutine();
		this.eventListenerService.clickToSelectNodeRoutine(this.select);
		this.eventListenerService.clickToSelectEdgeRoutine(this.select);
		this.eventListenerService.contextClickToDeleteNodeRoutine(this.select);
		this.eventListenerService.contextClickToDeleteEdgeRoutine(this.select);
		this.eventListenerService.dragAndDropToCreateEdgeRoutine();
	}

	/**
	 * Method provided by OnChanges interface.
	 * This method is called for every time we modify @Input variable content.
	 * @method Components.GraphComponent#ngOnChanges
	 * @param changes an object containing all @Input variables
	 */
	ngOnChanges(changes: any) {
		for(var change in changes) {
			// If some changes are detected in currentSrv (input field on the app form)
			if(change == 'currentSrv') {
				// Must do this because at initialisation, ngOnChanges is called and so currentSrv is not defined
				if(changes['currentSrv'].isFirstChange()) {
					return;
				}

				this.styleUpdater.updateFontSizeNodeLabel(changes, change);
				
				// Trick to update elements on the graph (every time we enter or remove a character on srv field)
				var elements = this.graph.cy.elements().remove();
				elements.restore();

			}

			// If some changes are detected in currentSrv (radio box on the app form)
			else if(change == 'currentTypeControl') {
				if(changes['currentTypeControl'].isFirstChange()) {
					return;
				}
				
				// Get id of current edge
				var id = this.graph.cy.edges('.selected-edge').id();
				this.graph.cy.getElementById(id).data('type_control', this.currentTypeControl);

				// update css style accordingly to their type (data or control)
				var controlEdges = this.graph.cy.elements('edge[type_control = 0]');
				var dataEdges = this.graph.cy.elements('edge[type_control = 1]');
				controlEdges.addClass('edge-control');
				dataEdges.removeClass('edge-control');
			}

			else if(change == 'importData') {
				if(changes['importData'].isFirstChange()) {
					return;
				}
				this.graph.clearGraph();
				this.json.createGraphOnImport(this.importData);
				this.styleUpdater.updateStyleEdgesControl();
				this.styleUpdater.updateStyleEdgesAlternative();
				this.styleUpdater.hideAlternatives();
				this.graph.cy.fit(50);
			}

			// If some changes are detected in currentAlt (input field on the app form)
			if(change == 'currentAlt') {
				// Must do this because at initialisation, ngOnChanges is called and so currentAlt is not defined
				if(changes['currentAlt'].isFirstChange()) {
					return;
				}

				var id = this.graph.cy.nodes('.selected-node').id();
				var currentService = this.graph.cy.getElementById(id);

				if(!isNaN(parseInt(changes['currentAlt'].currentValue)) || changes['currentAlt'].currentValue == undefined || changes['currentAlt'].currentValue == "") {
					this.styleUpdater.updateAlternativesCurrentService(currentService);
				}

			}
		}
	}

	/**
	 * Launch the workflow.
	 * Multiple preconditions to launch it :
	 * 1) The workflow must have a name
	 * 2) All services must have a srv field ({@link Service})
	 * 3) The workflow must have at least one service
	 * 4) The workflow must not have cycle
	 * 
	 * After checking these 4 preconditions, the cytoscape graph is converted into a specific ginflow JSON representation.
	 * Then this JSON is sent through a WebSocket to the Java-API ginflow.
	 * Finally, we disable update of the graph when the workflow is finished.
	 * @method Components.GraphComponent#onLaunch
	 */
	public onLaunch() {
		var numberOfServices = this.graph.getNodes().length;
		this.styleUpdater.clearStyleSelectedItems();

		// Send message to the parent component to inform that the user has clicked on export button
		this.launch.emit("export");

		// Workflow has no name
		if(!this.graph.isWorkflowWithName(this.workflowName)) {
			return;
		}

		// One or more services have no name
		if(!this.graph.isAllServicesWithSrv()) {
			return;
		}
		
		// The workflow has no services
		if(this.graph.isWorkflowWithNoService()) {
			return;
		}

		// The graph has no root (cycle)
		if(!this.graph.isGraphWithRoots()) {
			return;
		}

		// // Get nodes which have already been visited
		// var edges_visited = this.graph.cy.edges("[?visited]");
		// // Reset flag visited and cycle for all edges of the graph
		// for(var i = 0; i < edges_visited.length; i++) {
		// 	edges_visited[i].data('visited', null);
		// 	edges_visited[i].data('cycle', null);
		// }
		
		// // Browse the graph for each root and detect if it has a cycle
		// for(var i = 0; i < this.graph.cy.nodes().roots().length; i++) {
		// 	var root = this.graph.cy.nodes().roots()[i];
		// 	edges_visited.data('visited', null);
		// 	edges_visited.data('cycle', null);
		// 	if(this.graph.isGraphCyclic(root)) {
		// 		console.error("Cyclic graph");
		// 		return;
		// 	}
		// }

		// Remove all dependencies of each services
		this.dependenciesUpdater.clearServiceDependencies();

		// Then update their dependencies
		this.dependenciesUpdater.updateServiceDependencies();

		this.dependenciesUpdater.removeAlternativesServicesFromServicesDependencies();

		// Build the JSON object of the graph for ginflow
		var json = this.json.cytoscapeGraphToGinflowWorkspaceJson(this.workflowName);
		console.log(json);

		// Send to Ginflow the workspace
		this.webSocketService.sendData(json);
		this.wasClickedOnce = true;

		// // Disable extension
		// // NB: This method does not work but the flag 'enabled' is used as test for event listeners
		// // NB2: If enabled is false it will disabled creation and deletion of elements in the graph
		// this.graph.cy.edgehandles('option', 'enabled', false);

		// // Send the map of services which contain their result and send it to AppComponent
		// this.end.emit(this.ginflowNotifier.services_map);

		// Hide alternatives
		this.styleUpdater.hideAlternatives();
	}

	/**
	 * When clicking on the button, it allows to switch mode between edit mode and export mode.
	 * @method Components.GraphComponent#onClickToggle
	 */
	public onClickToggle() {
		this.importJSON = !this.importJSON;
		this.exportJSON = false;

		// Send to AppComponent booleans to know what component will be displayed and what component will be hidden
		this.toggle.emit({
			value: {
				load: this.importJSON,
				exportJSON: this.exportJSON
			}
		});
	}

	/**
	 * Called when clicking on export JSON button.
	 * @method Components.GraphComponent#onExportJSON
	 */
	public onExportJSON() {
		this.exportJSON = true;
		this.importJSON = false;
		this.hideImportJSON = true;
		
		// Get nodes of the graph
		var services = this.graph.cy.nodes();

		// Remove all dependencies of services
		this.dependenciesUpdater.clearServiceDependencies();

		// Then update them
		this.dependenciesUpdater.updateServiceDependencies();

		// Build the JSON object
		this.exportData = this.json.graphToJson(this.workflowName, services);

		// Send to AppComponent the built JSON object
		this.exports.emit({
			value: {
				isExportJSON: this.exportJSON,
				data: this.exportData,
				isJSONFormHidden: this.hideImportJSON,
				isImportJSON: this.importJSON
			}
		});
	}
}