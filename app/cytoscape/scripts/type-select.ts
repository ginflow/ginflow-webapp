/**
 * Enum which concerns the type of selection in the graph.
 * @typedef {number} Enums.TypeSelect
 */
export enum TypeSelect {
	NodeSelection,
	NodeDeselection,
	EdgeSelection,
	EdgeDeselection
}