## Installation

- Clone the git repo where you want
	git clone git@bitbucket.org:ginflow/ginflow-webapp.git

- On the ginflow-webapp folder, install required dependencies
	npm install

## Launch

- On the ginflow-webapp folder
	npm start to start the application locally

## Generate documentation

- On the ginflow-webapp folder
	jsdoc ./app/cytoscape/scripts -c ./tsdoc.json -d ./docs README.md -t ./node_modules/ink-docstrap/template -r
